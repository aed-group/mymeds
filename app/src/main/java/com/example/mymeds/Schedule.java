package com.example.mymeds;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class Schedule {
    private ArrayList<Calendar> hour;               //HH:mm
    private int interval;

    @Override
    public String toString() {
        return "Schedule{" +
                "hour=" + hour +
                ", interval=" + interval +
                '}';
    }

    public Schedule(ArrayList<Calendar> hour,int interval) {
        this.hour = hour;
        this.interval = interval;
        buildHour();

    }

    public Schedule(Calendar initial_date, int interval) {
        this.hour = new ArrayList<>();
        hour.add(initial_date);
        this.interval = interval;
        buildHour();
    }

    public static Schedule ScheduleFactory(String initial_hour, String initial_minutes, int interval) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(initial_hour));
        c.set(Calendar.MINUTE, Integer.parseInt(initial_minutes));

        return new Schedule(c, interval);
    }

    public static Schedule ScheduleFactory(int initial_hour, int initial_minutes, int interval) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, initial_hour);
        c.set(Calendar.MINUTE, initial_minutes);

        return new Schedule(c, interval);
    }

    public void buildHour(){
        Calendar now = Calendar.getInstance();
        for(int i = 0; i<this.hour.size(); i++)
            if (!hour.get(i).after(now))
                hour.get(i).set(Calendar.DAY_OF_WEEK, hour.get(i).get(Calendar.DAY_OF_WEEK) + 1);
    }


    public Calendar getNextHour(){

        Calendar toReturn = this.hour.get(0);

        for (int i = 0; i<this.hour.size();i++) {
            if (toReturn.after(this.hour.get(i)))
                toReturn = this.hour.get(i);
        }

        return toReturn;
    }

    public void upDateHour(){
        Calendar now = Calendar.getInstance();
        now.add(Calendar.SECOND, 10);; //Just to be sure

        for (int i = 0; i<this.hour.size(); i++) {
            if (now.after(this.hour.get(i)))
                this.hour.get(i).add(Calendar.HOUR_OF_DAY, interval);
        }
    }

    public boolean isToday(){
        Calendar now = Calendar.getInstance();
        Calendar midnight = Calendar.getInstance();
        midnight.set(Calendar.HOUR_OF_DAY,23);
        midnight.set(Calendar.MINUTE,59);

        if (getNextHour().after(now) && midnight.after(getNextHour()))  //between now and midnight
            return true;
        return false;
    }
}
