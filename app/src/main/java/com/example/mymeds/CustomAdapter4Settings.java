package com.example.mymeds;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.zip.Inflater;

public class CustomAdapter4Settings extends BaseAdapter {
    Context context;
    ArrayList<User> u;
    LayoutInflater inflter;

    public CustomAdapter4Settings(Context applicationContext, ArrayList<User> u) {
        this.context = applicationContext;
        this.u = u;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return u.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.listviewacc, null);
        TextView acc = (TextView) view.findViewById(R.id.account);

        acc.setText( u.get(i).getEmail());

        return view;

    }
}