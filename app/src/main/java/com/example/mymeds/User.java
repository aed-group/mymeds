package com.example.mymeds;

import android.provider.CalendarContract;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;

import com.example.mymeds.Med;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class User {
    private ArrayList<Med> mymeds;
    private String name;
    private String email;

    public User(ArrayList<Med> mymeds, String name, String email) {
        this.mymeds = mymeds;
        this.name = name;
        this.email = email;
    }

    public User (String name, String email){
        this(new ArrayList<Med>(), name, email);
    }

    @Override
    public String toString() {
        return "User{" +
                "mymeds=" + mymeds +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public void addMed(Med m){
        this.mymeds.add(m);
    }

    public void removeMed(Med m){ this.mymeds.remove(m);}

    public Med getmymeds(int i){
        return mymeds.get(i);
    }

    public ArrayList<Med> getmymeds(){ return this.mymeds; }

    public void updateMed(int i, int newstock){
        this.mymeds.get(i).stock += newstock;
    }

    public int numMeds(){ return this.mymeds.size();}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void debugSort(){
        for(int i=0; i<mymeds.size();i++){
            System.out.println(mymeds.get(i) + " " + mymeds.get(i).time.getNextHour() );
        }
    }

    public void sortMeds(){
        mymeds.sort(new Comparator<Med>() {
            @Override
            public int compare(Med o1, Med o2) {
                if(o1.time.getNextHour().after(o2.time.getNextHour()))
                    return 1;
                return -1;
            }
        });
    }

    public ArrayList<Med> getMedsOfToday(){
        ArrayList<Med> todayInTake = new ArrayList<>();

        for (int i = 0; i< mymeds.size();i++){
            if (mymeds.get(i).time.isToday())
                todayInTake.add(mymeds.get(i));
        }

        return todayInTake;
    }


    public String toJSON() {
        /* JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("mymeds", this.getmymeds());
            jsonObject.put("name", this.getName());
            jsonObject.put("email", this.getEmail());

            System.out.println(jsonObject.toString());
            return jsonObject.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        } */
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static User fromJSON(String JSONsource) {
        /* try {
            JSONObject json_src = (JSONObject) new JSONTokener(JSONsource).nextValue();

            String json_mymeds = (String) json_src.getJSONObject("mymeds");
            String json_name = (String) json_src.get("name");
            String json_email = (String) json_src.get("email");

            return new User(json_mymeds, json_name, json_email);
        } catch (JSONException e) {
            e.printStackTrace();
        } */
        Gson gson = new Gson();
        return gson.fromJson(JSONsource, User.class);
    }

}
