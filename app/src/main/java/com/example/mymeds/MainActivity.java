package com.example.mymeds;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.mymeds.CustomAdapter;
import com.example.mymeds.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
                   Home.OnFragmentInteractionListener,
                   EditMeds.OnFragmentInteractionListener,
                   AddMeds.OnFragmentInteractionListener,
                    AddPill.OnFragmentInteractionListener,
                    Settings.OnFragmentInteractionListener {

    private View navHeader;

    public User user;

    public HashMap<String, Object> msg_stack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navHeader = navigationView.getHeaderView(0);

        try {
            user = getUserFromFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        navigationView.setCheckedItem(R.id.nav_MyMeds);

        msg_stack = new HashMap<>();

        Fragment fragment = new Home();
        displaySelectedFragment(fragment);

    }



    @Override
    public void onBackPressed() {
        // DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        // if (drawer.isDrawerOpen(GravityCompat.START)) {
        //     drawer.closeDrawer(GravityCompat.START);
        // } else {
        //     super.onBackPressed();
        // }

        List fragmentList = getSupportFragmentManager().getFragments();

        boolean handled = false;
        for(Object f : fragmentList) {
            if(f instanceof BaseFragment) {
                handled = ((BaseFragment)f).onBackPressed();

                if(handled) {
                    break;
                }
            }
        }

        if(!handled) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment;

        if (id == R.id.nav_MyMeds) {
            fragment = new Home();
            displaySelectedFragment(fragment);
        } else if (id == R.id.nav_addbox) {
            fragment = new AddPill();
            displaySelectedFragment(fragment);
        } else if (id == R.id.nav_addmed) {
            fragment = new AddMeds();
            displaySelectedFragment(fragment);
        } else if (id == R.id.nav_Historico) {

        } else if (id == R.id.nav_definitions) {
            fragment = new Settings();
            displaySelectedFragment(fragment);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onStop() {
        try {
            saveUserOnFile(user);
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onStop();
    }

    /**
     * Loads the specified fragment to the frame
     *
     * @param fragment
     */
    public void displaySelectedFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri){
        //you can leave it empty
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void updateNavDrawer(int id) {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        System.out.println(navigationView);
        navigationView.setCheckedItem(id);
    }

    public void saveUserOnFile(User user) throws IOException {
        File path = getApplicationContext().getFilesDir();
        File file = new File(path, "user_data.mymeds");

        FileOutputStream stream = new FileOutputStream(file);
        try {
            stream.write(getUser().toJSON().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            stream.close();
        }
    }

    public User getUserFromFile() throws IOException {
        File path = getApplicationContext().getFilesDir();
        File file = new File(path, "user_data.mymeds");

        if(!file.exists())
            return createNewUser();

        int length = (int) file.length();

        byte[] bytes = new byte[length];

        FileInputStream in = new FileInputStream(file);
        try {
            in.read(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            in.close();
        }

        String contents = new String(bytes);
        return User.fromJSON(contents);
    }

    public User createNewUser() {


        /*Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 13);
        c.set(Calendar.MINUTE, 30);*/

        //ArrayList<Calendar> dates = new ArrayList<Calendar>();
        //dates.add(c);

        //Schedule s = new Schedule(dates, 12);

        User u = new User("user1", "user1@gmail.com");
        /*Med m = new Med("XANAX", s, 10, 2, 0);
        u.addMed(m);*/

        return u;
    }

    public void putMessage(String type, Object o) {
        msg_stack.put(type, o);
    }

    public Object getMessage(String type) {
        return msg_stack.get(type);
    }

}
