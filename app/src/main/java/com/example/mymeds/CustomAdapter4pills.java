package com.example.mymeds;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import java.text.ParseException;
import java.util.ArrayList;
import java.util.zip.Inflater;

public class CustomAdapter4pills extends BaseAdapter {
    final Context context;
    final User u;
    LayoutInflater inflter;

    public CustomAdapter4pills(Context applicationContext, User u) {
        this.context = applicationContext;
        this.u = u;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return u.numMeds();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.listviewaddpill, null);
        TextView name = (TextView) view.findViewById(R.id.Name);
        name.setText( u.getmymeds(i).name);

        final EditText number = (EditText) view.findViewById(R.id.Npills);
        Button addpill = (Button) view.findViewById(R.id.buttonAddPill);

        addpill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                u.updateMed(i,Integer.parseInt(number.getText().toString()));
                Toast.makeText(context, "Pills Added", Toast.LENGTH_SHORT).show();
            }
        });

        return view;

    }
}