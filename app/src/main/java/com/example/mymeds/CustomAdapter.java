package com.example.mymeds;

import android.content.Context;
import android.media.Image;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.zip.Inflater;

public class CustomAdapter extends BaseAdapter {
    Context context;
    ArrayList<Med> mymeds;
    LayoutInflater inflter;
    ArrayList<String> error;
    
    public CustomAdapter(Context applicationContext, ArrayList<Med> mymeds) {
        this.context = applicationContext;
        this.mymeds = mymeds;
        inflter = (LayoutInflater.from(applicationContext));
        this.error = new ArrayList<>();
        error.add("No meds Here!");
    }

    @Override
    public int getCount() {
        if (mymeds.size() != 0)
            return mymeds.size();
        else
            return 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        if (mymeds.size() != 0) {
            view = inflter.inflate(R.layout.listview, null);

            TextView name = (TextView) view.findViewById(R.id.name);
            TextView especificacao = (TextView) view.findViewById(R.id.next);
            ImageView icon = (ImageView) view.findViewById(R.id.icon);
            TextView stock = (TextView) view.findViewById(R.id.stock);

            name.setText(mymeds.get(i).name);
            especificacao.setText(mymeds.get(i).showNexthour());
            icon.setImageResource(mymeds.get(i).photo);
            stock.setText("Your available stock: " + mymeds.get(i).stock);
        }
        else{
            view = inflter.inflate(R.layout.listview_errormsg, null);

            TextView errormsg = (TextView) view.findViewById(R.id.errormsg);
            errormsg.setText(error.get(i));
        }
        
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) context).putMessage("home_to_edit", mymeds.get(i));
                Fragment fragment = new EditMeds();
                ((MainActivity) context).displaySelectedFragment(fragment);
            }
        });

        return view;

    }

}