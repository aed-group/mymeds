package com.example.mymeds;

import android.support.v4.app.Fragment;

public class BaseFragment extends Fragment {

    public boolean onBackPressed() {
        return true;
    }
}
