package com.example.mymeds;

import java.util.Calendar;

public class Med {
    public String name;
    public Schedule time;
    public int stock;
    public int toTake;
    public int photo = R.drawable.pill;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Schedule getTime() {
        return time;
    }

    public void setTime(Schedule time) {
        this.time = time;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getToTake() {
        return toTake;
    }

    public void setToTake(int toTake) {
        this.toTake = toTake;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public Med(String name, Schedule time, int stock, int toTake, int photo){
        this.name = name;
        this.time = time;
        this.stock = stock;

        if (photo != 0)
            this.photo = photo;

        this.toTake = toTake;
    }

    public String showNexthour(){
        String weekday;
        Calendar c =this.time.getNextHour();
        switch (c.get(Calendar.DAY_OF_WEEK)){
            case 1:
                weekday = "Sunday";
                break;
            case 2:
                weekday = "Monday";
                break;
            case 3:
                weekday = "Tuesday";
                break;
            case 4:
                weekday = "Wednesday";
                break;
            case 5:
                weekday = "Thursday";
                break;
            case 6:
                weekday = "Friday";
                break;
            case 7:
                weekday = "Saturday";
                break;
            default:
                weekday = "ERROR";
        }
        return "Next intake: " + weekday+ ", " + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE);
    }

    @Override
    public String toString() {
        return "Med{" +
                "name='" + name + '\'' +
                ", time=" + time +
                ", stock=" + stock +
                ", toTake=" + toTake +
                ", photo=" + photo +
                '}';
    }
}
