package com.example.mymeds;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddMeds.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddMeds#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddMeds extends BaseFragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    Button addButton;

    EditText medName;
    EditText medNumberAtTime;
    EditText medNumberInitial;
    EditText datePicker;
    EditText repeat;

    User u;

    public AddMeds() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddMeds.
     */
    // TODO: Rename and change types and number of parameters
    public static AddMeds newInstance(String param1, String param2) {
        AddMeds fragment = new AddMeds();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        getActivity().setTitle("Add Medicine");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_meds, container, false);

        addButton = (Button) v.findViewById(R.id.addButton);
        addButton.setOnClickListener(this);
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        u = ((MainActivity)getActivity()).getUser();

        repeat = (EditText) view.findViewById(R.id.repeat);

        repeat.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRadioButtonDialog();
            }
        });

        /* // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                R.array.repeat_time_values, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        repeat.setAdapter(adapter); */

        datePicker = (EditText) view.findViewById(R.id.datePicker);

        datePicker.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showClockDialog();
            }

        });



    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onClick(View v) {
        medName = (EditText) ((EditText) getView().findViewById(R.id.medName));
        medNumberAtTime = (EditText) ((EditText) getView().findViewById(R.id.medNumberAtTime));
        medNumberInitial = (EditText) ((EditText) getView().findViewById(R.id.medNumberInitial));

        if (!checkInput(medName.getText().toString(), datePicker.getText().toString(), repeat.getText().toString(), medNumberAtTime.getText().toString(), medNumberInitial.getText().toString())) {
            Toast.makeText(getContext(), "Invalid input. Try again.", Toast.LENGTH_SHORT).show();
        return;
        }

        int hour = Integer.parseInt(datePicker.getText().toString().split(":")[0]);
        int minute = Integer.parseInt(datePicker.getText().toString().split(":")[1]);

        String repeat_text = repeat.getText().toString();
        int repeat_time;
        if(repeat_text.equals("No repeats"))
            repeat_time = 0;
        else
            repeat_time = Integer.parseInt(repeat_text.replaceAll("[\\D]", ""));

        if(repeat_text.contains("Days"))
            repeat_time *= 24;
        else if(repeat_text.contains("Weeks"))
            repeat_time *= 24 * 7;

        Schedule medSchedule = Schedule.ScheduleFactory(hour, minute, repeat_time);

        Med m = new Med(medName.getText().toString(), medSchedule, Integer.parseInt(medNumberInitial.getText().toString()), Integer.parseInt(medNumberAtTime.getText().toString()), 0);

        System.out.println(m);
        System.out.println(u);

        u.addMed(m);
        u.debugSort();
        u.sortMeds();
        u.debugSort();

        ((MainActivity)getActivity()).setUser(u);

        ((MainActivity)getActivity()).updateNavDrawer(R.id.nav_MyMeds);

        Fragment fragment = new Home();
        ((MainActivity)getActivity()).displaySelectedFragment(fragment);

    }

    @Override
    public void onPause() {
        ((MainActivity)getActivity()).setUser(u);
        super.onPause();
    }

    public void showClockDialog() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                datePicker.setText( selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        // mTimePicker.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        mTimePicker.show();
    }

    private void showRadioButtonDialog() {

        // custom dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radiobutton_dialog);

        final RadioGroup rg = (RadioGroup) dialog.findViewById(R.id.select_radio);

        final ArrayList<RadioButton> rb_list = new ArrayList<>();

        // get radio buttons
        final RadioButton rb_no_repeat = (RadioButton) dialog.findViewById(R.id.no_repeat);
        final RadioButton rb_each = (RadioButton) dialog.findViewById(R.id.each);
        final EditText each_edit_text = (EditText) dialog.findViewById(R.id.each_edit_text);
        final Spinner each_spinner = (Spinner) dialog.findViewById(R.id.each_spinner);

        // set spinner values
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                R.array.repeat_time_values, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        each_spinner.setAdapter(adapter);

        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog_local) {
                if (rb_no_repeat.isChecked())
                    repeat.setText(rb_no_repeat.getText().toString());
                else if (rb_each.isChecked())
                    repeat.setText("Each " + each_edit_text.getText().toString() + " " + each_spinner.getSelectedItem().toString());
            }
        });

    }

    public boolean checkInput(String med_name, String time, String repeats, String num_pills_time, String num_pills_now) {
        if(med_name.equals(""))
            return false;
        else if(time.equals("Time"))
            return false;
        else if(repeats.equals("Repeats"))
            return false;
        else if(num_pills_time.equals("Number of pills to take at a time"))
            return false;
        else if(num_pills_now.equals("Number of pills you own"))
            return false;

        return true;
    }

    @Override
    public boolean onBackPressed() {
        Fragment fragment = new Home();
        ((MainActivity)getActivity()).displaySelectedFragment(fragment);
        System.out.println("Pressed back");
        return true;
    }
}
